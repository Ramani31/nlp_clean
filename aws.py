import boto3


def changeWord(sentimentString):
    if(sentimentString=='POSITIVE'):
        return 'Positive'

    elif (sentimentString == 'NEGATIVE'):
        return 'Negative'

    elif (sentimentString == 'NEUTRAL'):
        return 'Neutral'

    else:
        return 'Mixed'


def DetectSentimentAWS(listOfString):
    client = boto3.client('comprehend')



    response = client.batch_detect_sentiment(TextList=listOfString, LanguageCode='en')
    # transform The result into input string for dictionary--> Stupid function



    # print(response)
    resultList = response['ResultList']
    resultDict = resultList[0]

    #print(resultDict)
    sentimentResult = resultDict['Sentiment']

    sentimentScoreDict = resultDict['SentimentScore']

    #print(sentimentScoreDict)
    sentiment_Result = changeWord(sentimentResult)
    #print(sentiment_Result, sentimentScoreDict[sentiment_Result])
    return sentiment_Result,sentimentScoreDict[sentiment_Result], sentimentScoreDict