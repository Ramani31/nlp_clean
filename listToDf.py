import pandas as pd

def dfToExcel(df):
    writer=pd.ExcelWriter('NLP_ESG_OP.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Scores' , index=False)
    writer.save()


def print_full(x):
    pd.set_option('display.max_columns', len(x))
    print(x)
    pd.reset_option('display.max_columns')

def list_to_df(tempList):
    tempDict={}
    i=0
    if(len(tempList)%4==0):
        while(i<len(tempList)):
            tempDict[tempList[i]]==[tempList[i+1], tempList[i+2], tempList[i+3]]
            i=i+4
    else:
        print("there is some problem with previous step")
        pass
    # resultDf=pd.DataFrame.from_dict(tempDict, orient='index')
    # resultDf=resultDf.reset_index()
    # resultDf=resultDf.rename(columns={'index':'keys_filename'})
    # print_full(resultDf)
    # dfToExcel(resultDf)
    print(tempDict)