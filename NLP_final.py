import pandas as pd
from NLP_Clean import aws
from NLP_Clean import listToDf
import indicoio
import re
import os
def print_full(x):
    pd.set_option('display.max_columns', len(x))
    print(x)
    pd.reset_option('display.max_columns')

indicoio.config.api_key='52e1a26cde75715bf26eee59357a9673'

resultList=[]

allPdfs=[]

for filename in os.listdir("."):
    #print(filename)
    if re.search(r'\.pdf$', filename):
        allPdfs.append(filename)
        print(filename)
        resultList.append(filename)

        text=indicoio.pdf_extraction(filename, images=True, metadata=True, text=True, tables=True)
        #print("utilizing indicoio API for text extraction")
        text_list=list(text.values())
        textStr=text_list[1]


        newTextDict={}
        newIndex=0
        newString=""
        totLen=0
        for index, line in enumerate(textStr.splitlines()):
            ls=line.strip()
            ln=len(ls)
            newString+=" "+ls
            totLen+=ln+1
            if(totLen>4000):
                ####Hypothesis: next line is not more than 1000 characters long

                newTextDict[newIndex]=newString
                newIndex+=1
                newString=""
                totLen=0
        if newString!="":
            newTextDict[newIndex]=newString
        newList=[]


        for key, value in newTextDict.items():
            #print(key, len(value), value)
            print(key)
            newList.append(value)
            SentimentString, SentimentScore, SentimentScoreDict=aws.DetectSentimentAWS(newList)
            print(SentimentString, SentimentScore, SentimentScoreDict)
            resultList.append(SentimentString)
            resultList.append(SentimentScore)
            resultList.append(SentimentScoreDict)

print("preparing list of result")
print(resultList)


# for index, item in enumerate(resultList):
#     print(index, item)
listToDf.list_to_df(resultList)
